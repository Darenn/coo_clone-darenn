class Thing(object):
	def __init__(self,volume,name=None):
		self.volume=volume
		self.name=name
	def getVolume(self):
		return self.volume
	def setName(self,name):
		self.name=str(name)
	def getName(self):
		return self.name
	def __repr__(self):
		return self.getName()
	def hasName(self,name):
		return self.getName()==name
		
	
		
