class Box(object):
	def __init__(self,state=True,capacity=None):
		self.__contents=[]
		self.__state=state # False veut dire fermé
		self.__capacity=capacity
	def add(self,content):
		""" Add content in the box"""
		self.__contents.append(content)
	def __contains__(self,content):
		return content in self.__contents
	def remove(self,content):
		""" Remove content of the box"""
		self.__contents= [ x for x in self.__contents if x != content ]
	def isOpen(self):
		return self.__state
	def open(self):
		self.__state=True
	def close(self):
		self.__state=False
	"""def actionLook(self):
		if not self.isOpen():
			return "La boîte est fermée."
		else :
			sentence= "La boîte contient : "
			for content in self.__contents:
				sentence+=str(content)
			return sentence[0:-2]"""
	def setCapacity(self,capacity):
		self.__capacity=capacity
	def getCapacity(self):
		return self.__capacity
	def hasRoomFor(self,thing):
		if self.getCapacity()==None:
			return True
		else:
			return thing.getVolume() < self.getCapacity()
			
	def actionAdd(self,thing):
		if not self.isOpen() and not self.hasRoomFor(thing):
			return False
		else:
			self.add(thing)
			return True
	def find(self,thingName):
		if not self.isOpen():
			return None
		for thing in self.__contents :
			if thing.getName() == thingName :
				return thing
		return None
		
		
""" pour exo 6 
yaml.load(open(filename)) voir tp3"""
