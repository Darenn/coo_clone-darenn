from box import *
from thing import *

def testBoite():
	b=Box()
	b.open()
	b.add('truc1')
	b.add('truc2')
	assert 'truc1' in b
	assert not 'truc3' in b
	b.remove('truc1')
	assert not 'truc1' in b
	
	b.close()
	assert not b.isOpen()
	b.open()
	assert b.isOpen()
	b.close()
	assert not b.isOpen()
	
#	assert b.actionLook() =="La boîte est fermée."
#	b.open()
#	b.add('truc1')
#	assert b.actionLook() == "La boîte contient : truc2, truc1"
	
	assert b.getCapacity()==None
	b.setCapacity(5)
	assert b.getCapacity()==5
	assert not b.getCapacity()==3
	assert not b.getCapacity()==None

def testHasRoomFor():
	b=Box()
	b2=Box()
	b.open()
	b.setCapacity(5)
	
	t=Thing(3)
	t2=Thing(8)
	assert b.hasRoomFor(t)	
	assert not b.hasRoomFor(t2)
	assert b2.hasRoomFor(t2)
	
def testActionAdd():
	b=Box()
	b.open()
	b.setCapacity(5)
	
	t=Thing(3)
	res=b.actionAdd(t)
	assert res
	
def testThing():
	t=Thing(5)
	assert t.getVolume()==5
	
	
	
